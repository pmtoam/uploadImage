package com.byl.uploadimage;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UploadUtils {

  /**
   *
   * Postman 请求 <br>
   * Charles 抓包 <br>
   *
   * 抓包结果 <br>
   *
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="imei"
   *
   * 12345678
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="imsi"
   *
   * 123456
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="fileName"
   *
   * testFileName
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="time"
   *
   * 2017-09-14 19:01:27
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="audioFileBody"
   *
   * testAudioFileBody
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="callNumber"
   *
   * 13455556666
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB
   * Content-Disposition: form-data; name="duration"
   *
   * 16
   * ------WebKitFormBoundaryTEgAnbftC8cvmEeB--
   *
   *
   * @param urlStr http://139.129.230.14:100/v1/audio
   * @param filePath ？
   *
   * @return {"msg":"操作成功！","success":true}
   *
   * @since 2017-09-14
   *
   */
  public static String multipartFormDataUpload(String urlStr, String filePath) {

    String result = null; // 返回请求结果
    String BOUNDARY = "----WebKitFormBoundaryTEgAnbftC8cvmEeB"; // 分隔符
    HttpURLConnection conn = null;

    try {

      // 发送字段定义
      String imei = "12345678";                   // String
      String imsi = "123456";                     // String
      String fileName = "testFileName";           // String
      String time = "2017-09-14 19:01:27";        // String
      String audioFileBody = "testAudioFileBody"; // String ?
      String callNumber = "13455556666";          // String
      String duration = "16";                     // String


      // 构造正文
      StringBuffer sb = new StringBuffer();

      // ## 可以使用for循环拼接 ##
      // 构造 字段 imei
      sb = sb.append("--");
      sb = sb.append(BOUNDARY);
      sb = sb.append("\r\n");
      sb = sb.append("Content-Disposition: form-data; name=\"imei\"\r\n\r\n");
      sb = sb.append(imei);
      sb = sb.append("\r\n");

      // 构造 字段 imsi
      sb = sb.append("--");
      sb = sb.append(BOUNDARY);
      sb = sb.append("\r\n");
      sb = sb.append("Content-Disposition: form-data; name=\"time\"\r\n\r\n");
      sb = sb.append(time);
      sb = sb.append("\r\n");

      // 构造 文件 audioFileBody
//      sb = sb.append("--");
//      sb = sb.append(BOUNDARY);
//      sb = sb.append("\r\n");
//      sb = sb.append("Content-Disposition: form-data; name=\"audioFileBody\"; filename=\"REC-2017-09-14-21-01-11.mp3\"\r\n");
//      sb = sb.append("Content-Type: application/octet-stream\r\n\r\n");


      byte[] data = sb.toString().getBytes();
//      byte[] file = null;
      byte[] end_data = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();


      URL url = new URL(urlStr);
      conn = (HttpURLConnection) url.openConnection();
//      conn.setConnectTimeout(5000);
//      conn.setReadTimeout(30000);
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setUseCaches(false);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Connection", "Keep-Alive");
      conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
      conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
      OutputStream out = new DataOutputStream(conn.getOutputStream());


      // TODO 1
      out.write(data);

      // TODO 2-1 write file (scheme 1)
//      out.write(file);

      // TODO 2-2 write file (scheme 2)
//      File file = new File(filePath);
//      DataInputStream in = new DataInputStream(new FileInputStream(file));
//      int bytes = 0;
//      byte[] bufferOut = new byte[1024];
//      while ((bytes = in.read(bufferOut)) != -1) {
//        out.write(bufferOut, 0, bytes);
//      }
//      in.close();

      // TODO 3
      out.write(end_data);


      out.flush();
      out.close();


      // 读取服务器响应
      StringBuffer buffer = new StringBuffer();
      BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
      String line = null;
      while ((line = reader.readLine()) != null) {
        buffer.append(line).append("\n");
      }
      result = buffer.toString();
      reader.close();


    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (conn != null) {
        conn.disconnect();
      }
    }

    return result;

  }

}
